import { mount } from '@vue/test-utils'
import InputField from '../AppInputField/index.vue'

describe('inputfield', () => {
  it('emits events with correct value', () => {
    const wrapper = mount(InputField, {
      propsData: {
        id: 'id',
        label: 'a label',
        value: 'inputvalue'
      }
    })

    wrapper.find('input').trigger('input')

    expect(wrapper.emitted()).toEqual({"input": [["inputvalue"]]})

    wrapper.find('input').trigger('change')

    expect(wrapper.emitted().change).toEqual([["inputvalue"]])

    wrapper.find('input').trigger('blur')

    expect(wrapper.emitted().blur).toEqual([["inputvalue"]])
  })
  it('is is disabled when disabled prop is true', () => {
    const wrapper = mount(InputField, {
      propsData: {

        id: 'id',
        label: 'a label',
        value: 'inputvalue',
        disabled: true
      }
    })

    expect(wrapper.find('input').attributes('disabled')).toEqual('disabled')
    expect(wrapper.find('input').attributes('readonly')).toEqual('readonly')
  })
  it('renders error', () => {
    const wrapper = mount(InputField, {
      propsData: {

        id: 'id',
        label: 'a label',
        value: 'inputvalue',
        error: 'error',
        valid: false
      }
    })

    expect(wrapper.find('.error').text()).toEqual('error')
  })

})