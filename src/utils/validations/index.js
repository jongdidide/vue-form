import { extend } from 'vee-validate';
import { email, required } from 'vee-validate/dist/rules';

extend('email', {
  ...email,
  message: 'Voer een geldig emailadres in'
});

extend('required_field', {
  ...required,
  message: (field) => {
    return `Vul je ${field} in`
  } 
})

extend('postalcode', {
  validate(value) {
    return RegExp('[1-9]{1}[0-9]{3} ?[a-zA-Z]{2}').test(value)
  },
  message: `Voer een geldige postcode in`
})