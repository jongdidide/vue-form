import VueRouter from 'vue-router'
import Form from '../views/Form/index.vue'
import Confirmation from '../views/Confirmation/index.vue'

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Form',
      component: Form
    },
    {
      path: '/submitted',
      name: 'Bevestiging',
      component: Confirmation
    }
  ]
})