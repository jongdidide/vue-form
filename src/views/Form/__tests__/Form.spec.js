import 'regenerator-runtime/runtime'
import { mount, createLocalVue } from '@vue/test-utils'
import Form from '../index.vue'
import axios from 'axios'
import '../../../utils/validations/index'
import flushPromises from 'flush-promises'
import { ValidationProvider, ValidationObserver } from 'vee-validate'

describe('form.vue', () => {
    const localVue = createLocalVue()

    localVue.component('ValidationProvider', ValidationProvider);
    localVue.component('ValidationObserver', ValidationObserver);

    const $router = {
        push: jest.fn()
    }

    jest.mock('axios')

    it('does not submit when fields are empty', () => {
        const wrapper = mount(Form, {
            localVue,
            mocks: {
                $router
            }
        })

        const button = wrapper.find('button')

        button.trigger('click')

        expect($router.push).not.toHaveBeenCalled()
    })

    it('toggles inputfields when address is not found', async () => {
        const spy = jest.spyOn(axios, 'get')

        spy.mockRejectedValue('rejected')

        const wrapper = mount(Form, {
            localVue,
            mocks: {
                $router
            },
            sync: false
        })

        wrapper.setData({
            form: {
                postalCode: '1234AB'
            }
        })
        
        const postcode = wrapper.find('#postcode')
        await postcode.trigger('focus')
        const validator = wrapper.vm.$refs.postcode
        await validator.validate()
        await postcode.trigger('blur')

        await flushPromises();

        expect(spy).toHaveBeenCalled()
    
        expect(wrapper.vm.disableAddressField).toBe(false)
    })

    it('submits when complete', async () => {
        
        const spy = jest.spyOn(axios, 'post')

        spy.mockResolvedValue('done')

        const wrapper = mount(Form, {
            localVue,
            mocks: {
                $router
            },
            sync: false
        })

        wrapper.setData({
            form: {
                initials: "AB",
                preposition: "de",
                lastName: "Test",
                postalCode: "1234AB",
                street: "straat",
                city: "stad",
                number: "5",
                email: "email@email.com"
              }
        })

        const button = wrapper.find('button')

        const initialen = wrapper.find('#initialen')
        await initialen.trigger('blur')

        button.trigger('click')

        await flushPromises()

        expect($router.push).toHaveBeenCalled()
        expect(spy).toHaveBeenCalled()

    })
})